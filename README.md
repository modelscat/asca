# README #

This repo contain files for doing Anova Simultanous Component Analysis [ASCA](https://en.wikipedia.org/wiki/ANOVA%E2%80%93simultaneous_component_analysis) including variance and statistical inference by permutation testing. 

## SETUP ##

Download _all_ the files to a folder and try to run the following toy example

## Minimal Example

### Import and scale data

```
#!matlab
load BeerData.mat
xs = scale(lgX.data); 
```

### Setup model

```
#!matlab
F = [lgX.class{1,1}' lgX.class{1,2}']; 
Flb = {lgX.classname{1,1};lgX.classname{1,2}}; 
M = [1 0; 0 1]; 
```

### Estimate Model including permutation testing

```
#!matlab
m2 = ASCAcat(xs,F,Flb,100,M);
```

### Plot results 

Here the ASCA results are compared with normal PCA results in terms of scores plot with color according to design factors

```
#!matlab
[u s v] = svds(xs,2); 
subplot(2,2,1); scatter(u(:,1),u(:,2),100,lgX.class{1,1},'filled'); title(lgX.classname{1,1})
subplot(2,2,2); scatter(u(:,1),u(:,2),100,lgX.class{1,2},'filled'); title(lgX.classname{1,2})
u = m2.Effects{1}.loads{1}; 
subplot(2,2,3); scatter(u(:,1),u(:,2),100,lgX.class{1,1},'filled'); title(lgX.classname{1,1})
u = m2.Effects{2}.loads{1}; 
subplot(2,2,4); scatter(u(:,1),u(:,2),100,lgX.class{1,2},'filled'); title(lgX.classname{1,2})
```

### Contact ###

* Repo admin: Morten Arendt Rasmussen: mortenr_at_food.ku.dk